"""
@Karolina Majka

Przy tworzeniu tego kodu korzystałam z kilku źródeł:
    https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 
    https://docs.python.org/3/library/functions.html
    https://eportal.pwr.edu.pl/pluginfile.php/395220/mod_resource/content/1/TP_wyklad.pdf
    https://eportal.pwr.edu.pl/pluginfile.php/395206/mod_resource/content/1/WdP_wyklad.pdf
    https://www.w3schools.com/python/python_getstarted.asp """
""" Definiuje klasę Wielomian."""
class Wielomian:    
    """ Definiuje konstruktor, który jest wywoływany przy tworzeniu nowego obiektu klasy Wielomian """  
    def __init__(self, wspolczynniki):
        """ Sprawdza, czy wszystkie elementy w liście wspolczynniki są liczbami (int lub float)."""
        if not all(isinstance(a, (int, float)) for a in wspolczynniki):
            """  Jeśli którykolwiek element nie jest liczbą, zgłasza wyjątek ValueError z odpowiednim komunikatem """
            raise ValueError("Wszystkie współczynniki muszą być liczbami.")
            """ Przypisuje listę współczynników do atrybutu obiektu. """
        self.wspolczynniki = wspolczynniki 
        """  Oblicza stopień wielomianu (liczba elementów w liście minus 1) i przypisuje go do atrybutu obiektu. """
        self.stopien = len(wspolczynniki) - 1 
    """ Definiuje metodę, która zwraca stopień wielomianu."""   
    def stopien_wielomianu(self):
        """ Zwraca wartość atrybutu stopien."""
        return self.stopien
    """  Definiuje metodę magiczną __str__, która zwraca tekstową reprezentację wielomianu. """    
    def __str__(self):
        """ Inicjalizuje string wyrazenie, który będzie reprezentacją wielomianu."""
        wyrazenie = "W(x) = "
        """  Iteruje przez listę współczynników, pobierając indeks i i wartość a. """
        for i, a in enumerate(self.wspolczynniki):
            """ Sprawdza, czy współczynnik a jest różny od zera """
            if a != 0:
                """ Dodaje do stringa wyrazenie współczynnik, zmienną x oraz potęgę x odpowiednio zredukowaną o indeks i. """
                wyrazenie += f"{a}x^{self.stopien - i} + "
                """ Zwraca wyrazenie bez ostatnich trzech znaków (+), aby uniknąć zbędnego plusa na końcu. """
        return wyrazenie[:-3]
    """ Definiuje metodę magiczną __call__, która umożliwia wywołanie obiektu klasy Wielomian jak funkcji. """
    def __call__(self, x):
        """ Inicjalizuje zmienną wartosc, która będzie przechowywać wynik obliczeń. """
        wartosc = 0
        """  Iteruje przez listę współczynników, pobierając indeks i i wartość a. """
        for i, a in enumerate(self.wspolczynniki): 
            """  Dodaje do wartosc wartość współczynnika a pomnożoną przez x podniesione do odpowiedniej potęgi """
            wartosc += a * (x ** (self.stopien - i))
            """  Zwraca obliczoną wartość wielomianu dla zadanej wartości x. """
        return wartosc
    """ Definiuje metodę __add__, która umożliwia dodawanie dwóch wielomianów."""
    def __add__(self, other):
        """ Sprawdza, czy other jest instancją klasy Wielomian."""
        if not isinstance(other, Wielomian):
            """  Jeśli other nie jest wielomianem, zwraca NotImplemented, co sygnalizuje, że operacja dodawania nie jest zaimplementowana dla tych typów. """
            return NotImplemented
        """ Oblicza maksymalną długość list współczynników dwóch wielomianów."""
        max_len = max(len(self.wspolczynniki), len(other.wspolczynniki))
        """ Dopasowuje długość listy współczynników self do max_len, dodając zera na początku."""
        wspolczynniki1 = [0] * (max_len - len(self.wspolczynniki)) + self.wspolczynniki
        """ Dopasowuje długość listy współczynników other do max_len, dodając zera na początku."""
        wspolczynniki2 = [0] * (max_len - len(other.wspolczynniki)) + other.wspolczynniki
        """ Tworzy nową listę współczynników, dodając odpowiednie elementy z wspolczynniki1 i wspolczynniki2. """
        suma_wspolczynnikow = [a + b for a, b in zip(wspolczynniki1, wspolczynniki2)]
        """ Tworzy nowy obiekt klasy Wielomian z listą sum współczynników i go zwraca."""
        return Wielomian(suma_wspolczynnikow)
    """ Definiuje metodę  __iadd__, która implementuje operator +=."""    
    def __iadd__(self, other):
        """ Wywołuje metodę __add__, aby dodać self i other """
        suma = self + other
        """ Aktualizuje współczynniki self wynikającymi ze suma."""
        self.wspolczynniki = suma.wspolczynniki
        """  Aktualizuje stopień self wynikającymi ze suma. """
        self.stopien = suma.stopien
        """  Zwraca zaktualizowany obiekt self. """
        return self
    """ Definiuje metodę magiczną __sub__, która umożliwia odejmowanie dwóch wielomianów. """
    def __sub__(self, other):
        """ Sprawdza, czy other jest instancją klasy Wielomian. """
        if not isinstance(other, Wielomian):
            """  Jeśli other nie jest wielomianem, zwraca NotImplemented, co sygnalizuje, że operacja odejmowania nie jest zaimplementowana dla tych typów. """
            return NotImplemented
        """ Oblicza maksymalną długość list współczynników dwóch wielomianów. """
        max_len = max(len(self.wspolczynniki), len(other.wspolczynniki))
        """ Dopasowuje długość listy współczynników self do max_len, dodając zera na początku. """ 
        wspolczynniki1 = [0] * (max_len - len(self.wspolczynniki)) + self.wspolczynniki
        """ Dopasowuje długość listy współczynników other do max_len, dodając zera na początku. """
        wspolczynniki2 = [0] * (max_len - len(other.wspolczynniki)) + other.wspolczynniki
        """ Tworzy nową listę współczynników, odejmując odpowiednie elementy z wspolczynniki1 i wspolczynniki2. """
        roznica_wspolczynnikow = [a - b for a, b in zip(wspolczynniki1, wspolczynniki2)]
        """ Tworzy nowy obiekt klasy Wielomian z listą różnic współczynników i go zwraca. """
        return Wielomian(roznica_wspolczynnikow)
    """ Definiuje metodę magiczną __isub__, która implementuje operator -=. """ 
    def __isub__(self, other):
        """ Wywołuje metodę __sub__, aby odjąć other od self. """
        roznica = self - other
        """ Aktualizuje współczynniki self wynikającymi ze roznica. """
        self.wspolczynniki = roznica.wspolczynniki
        """  Aktualizuje stopień self wynikającymi ze roznica. """
        self.stopien = roznica.stopien
        """ Zwraca zaktualizowany obiekt self """
        return self
    """ Definiuje metodę magiczną __mul__, która umożliwia mnożenie dwóch wielomianów. """
    def __mul__(self, other):
        """ Sprawdza, czy other jest instancją klasy Wielomian. """
        if not isinstance(other, Wielomian):
            """ Jeśli other nie jest wielomianem, zwraca NotImplemented, co sygnalizuje, że operacja mnożenia nie jest zaimplementowana dla tych typów. """
            return NotImplemented
        """ Oblicza stopień wynikowego wielomianu jako sumę stopni self i other. """
        stopien_wynikowy = self.stopien + other.stopien
        """  Tworzy listę zer o długości równej stopniowi wynikowego wielomianu plus jeden. """
        wynikowe_wspolczynniki = [0] * (stopien_wynikowy + 1)
        """ Iteruje przez listę współczynników self, pobierając indeks i i wartość a. """
        for i, a in enumerate(self.wspolczynniki):
            """ Iteruje przez listę współczynników other, pobierając indeks j i wartość b. """
            for j, b in enumerate(other.wspolczynniki):
                """ Mnoży współczynniki a i b i dodaje wynik do odpowiedniego elementu listy wynikowe_wspolczynniki """
                wynikowe_wspolczynniki[i + j] += a * b
                """ Tworzy nowy obiekt klasy Wielomian z listą wynikowych współczynników i go zwraca. """
        return Wielomian(wynikowe_wspolczynniki)
    """ Definiuje metodę magiczną __imul__, która implementuje operator *=. """ 
    def __imul__(self, other):
        """ Wywołuje metodę __mul__, aby pomnożyć self przez other. """
        iloczyn = self * other
        """ Aktualizuje współczynniki self wynikającymi z iloczyn. """
        self.wspolczynniki = iloczyn.wspolczynniki
        """ Aktualizuje stopień self wynikającymi z iloczyn. """
        self.stopien = iloczyn.stopien
        """ Zwraca zaktualizowany obiekt self. """
        return self
""" TESTY """
""" Tworzy obiekt w1 klasy Wielomian reprezentujący wielomian 1x^2 + 2x + 3."""
w1 = Wielomian([1, 2, 3])  
""" Tworzy obiekt w2 klasy Wielomian reprezentujący wielomian 4x^2 - 1. """
w2 = Wielomian([4, 0, -1])  
""" Wywołuje metodę __str__ obiektu w1 i drukuje jego tekstową reprezentację. """
print(w1) 
""" Wywołuje metodę __str__ obiektu w2 i drukuje jego tekstową reprezentację. """ 
print(w2)  
""" Drukuje stopień wielomianu w1. """
print(w1.stopien_wielomianu())  
""" Drukuje stopień wielomianu w2. """
print(w2.stopien_wielomianu())  
""" Wywołuje metodę __call__ obiektu w1 z x = 2 i drukuje wynik. """
print(w1(2))  
""" Wywołuje metodę __call__ obiektu w2 z x = 2 i drukuje wynik.  """
print(w2(2))  
""" Dodaje wielomiany w1 i w2, tworząc nowy obiekt w3. """
w3 = w1 + w2
""" Wywołuje metodę __str__ obiektu w3 i drukuje jego tekstową reprezentację. """
print(w3)  
""" Odejmują wielomiany w1 i w2, tworząc nowy obiekt w4. """
w4 = w1 - w2
""" Wywołuje metodę __str__ obiektu w4 i drukuje jego tekstową reprezentację. """
print(w4)  
""" Mnoży wielomiany w1 i w2, tworząc nowy obiekt w5. """
w5 = w1 * w2
"""  Wywołuje metodę __str__ obiektu w5 i drukuje jego tekstową reprezentację """
print(w5)  
""" Dodaje wielomian w2 do w1 i aktualizuje w1. """
w1 += w2
""" Wywołuje metodę __str__ obiektu w1 i drukuje jego tekstową reprezentację. """
print(w1) 
""" Odejmuje wielomian w2 od w1 i aktualizuje w1. """ 
w1 -= w2
""" Wywołuje metodę __str__ obiektu w1 i drukuje jego tekstową reprezentację. """
print(w1)  
""" Mnoży wielomian w2 przez w1 i aktualizuje w1. """
w1 *= w2
""" Wywołuje metodę __str__ obiektu w1 i drukuje jego tekstową reprezentację. """
print(w1)  
