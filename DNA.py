
"""
@author: Karolina Majka

Przy tworzeniu tego kodu korzystałam z kilku źródeł:
    https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 
    https://docs.python.org/3/library/functions.html
    https://eportal.pwr.edu.pl/pluginfile.php/395220/mod_resource/content/1/TP_wyklad.pdf
    https://eportal.pwr.edu.pl/pluginfile.php/395206/mod_resource/content/1/WdP_wyklad.pdf
    https://www.w3schools.com/python/python_getstarted.asp 
"""
""" Definiowanie klasy DNASequence """
class DNASequence:
    """ Zbiór dozwolonych znaków w sekwencji DNA """
    VALID_CHARS = {'A', 'T', 'C', 'G'}

    """ Metoda inicjalizacyjna klasy 
    https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 86, 83 """
    def __init__(self, identifier, data):
        """ Przypisanie identyfikatora sekwencji """
        self.identifier = identifier
        """ Przypisanie sekwencji zasad DNA """
        self.data = data
        """ Obliczenie długości sekwencji
        https://docs.python.org/3/library/functions.html#len """
        self.length = len(data)
        """ Sprawdzanie poprawności sekwencji """
        self.validate_sequence()  

    """ Metoda sprawdzająca poprawność sekwencji """
    def validate_sequence(self):
        """ Sprawdzanie, czy każda zasada w sekwencji należy do dozwolonych znaków """
        if not all(char in self.VALID_CHARS for char in self.data):
            """ Wyjątek w przypadku niedozwolonych znaków """
            raise ValueError("Sekwencja DNA zawiera niedozwolone znaki.") 

    """ Metoda zwracająca sekwencję w formacie FASTA 
    https://docs.python.org/3/library/functions.html#func-str """
    def __str__(self):
        """ Format FASTA: identyfikator na pierwszej linii, sekwencja na drugiej """
        return f">{self.identifier}\n{self.data}"  

    """ Metoda do mutowania sekwencji """
    def mutate(self, position, value):
        """ Sprawdzanie, czy nowy znak jest dozwolony """
        if value not in self.VALID_CHARS:
            """ Wyjątek w przypadku niedozwolonego znaku """
            raise ValueError("Niedozwolony znak do mutacji.")
        """ Zmiana zasady na nową wartość w podanej pozycji """
        self.data = self.data[:position] + value + self.data[position + 1:]

    """ Metoda do wyszukiwania motywu w sekwencji """
    def findMotif(self, motif):
        """ Zwraca pozycję początkową motywu """
        return self.data.find(motif) 

    """ Metoda do tworzenia nici komplementarnej """
    def complement(self):
        """ Słownik zasad komplementarnych """
        complement = {'A': 'T', 'T': 'A', 'C': 'G', 'G': 'C'}
        """ Tworzenie nici komplementarnej poprzez zamianę każdej zasady na jej komplement """
        comp_seq = ''.join(complement[base] for base in self.data)
        """ Zwraca nową sekwencję DNA """
        return DNASequence(self.identifier + "_komplementarna", comp_seq) 

    """ Metoda do transkrypcji sekwencji DNA do RNA """
    def transcribe(self):
        """ Zamiana tyminy (T) na uracyl (U) w sekwencji """
        rna_data = self.data.replace('T', 'U')
        """ Zwraca nową sekwencję RNA """
        return RNASequence(self.identifier + "_transkrybowana", rna_data)  


""" Definiowanie klasy RNASequence """
class RNASequence:
    """ Zbiór dozwolonych znaków w sekwencji RNA """
    VALID_CHARS = {'A', 'U', 'C', 'G'}

    """ Metoda inicjalizacyjna klasy """
    def __init__(self, identifier, data):
        """ Przypisanie identyfikatora sekwencji """
        self.identifier = identifier  
        """ Przypisanie sekwencji zasad RNA """
        self.data = data  
        """ Obliczenie długości sekwencji """
        self.length = len(data)  
        """ Sprawdzanie poprawności sekwencji """
        self.validate_sequence()  

    """ Metoda sprawdzająca poprawność sekwencji """
    def validate_sequence(self):
        """ Sprawdzanie, czy każda zasada w sekwencji należy do dozwolonych znaków """
        if not all(char in self.VALID_CHARS for char in self.data):
            """ Wyjątek w przypadku niedozwolonych znaków """
            raise ValueError("Sekwencja RNA zawiera niedozwolone znaki.")  

    """ Metoda zwracająca sekwencję w formacie FASTA """
    def __str__(self):
        """ Format FASTA: identyfikator na pierwszej linii, sekwencja na drugiej """
        return f">{self.identifier}\n{self.data}"  

    """ Metoda do mutowania sekwencji """
    def mutate(self, position, value):
        """ Sprawdzanie, czy nowy znak jest dozwolony """
        if value not in self.VALID_CHARS:
            """ Wyjątek w przypadku niedozwolonego znaku """
            raise ValueError("Niedozwolony znak do mutacji.")  
            
        """ Zmiana zasady na nową wartość w podanej pozycji """
        self.data = self.data[:position] + value + self.data[position + 1:]

    """ Metoda do wyszukiwania motywu w sekwencji """
    def findMotif(self, motif):
        """ Zwraca pozycję początkową motywu """
        return self.data.find(motif)  
    """ Metoda do translacji sekwencji RNA do białka """
    def translate(self):
        """ Prosta tabela kodonów (triplet zasad RNA -> aminokwas) """
        codon_table = {
            'AUG': 'M', 'UUU': 'F', 'UUC': 'F', 'UUA': 'L', 'UUG': 'L',
            'CUU': 'L', 'CUC': 'L', 'CUA': 'L', 'CUG': 'L', 'AUU': 'I',
            'AUC': 'I', 'AUA': 'I', 'GUU': 'V', 'GUC': 'V', 'GUA': 'V',
            'GUG': 'V', 'UCU': 'S', 'UCC': 'S', 'UCA': 'S', 'UCG': 'S',
            'CCU': 'P', 'CCC': 'P', 'CCA': 'P', 'CCG': 'P', 'ACU': 'T',
            'ACC': 'T', 'ACA': 'T', 'ACG': 'T', 'GCU': 'A', 'GCC': 'A',
            'GCA': 'A', 'GCG': 'A', 'UAU': 'Y', 'UAC': 'Y', 'CAU': 'H',
            'CAC': 'H', 'CAA': 'Q', 'CAG': 'Q', 'AAU': 'N', 'AAC': 'N',
            'AAA': 'K', 'AAG': 'K', 'GAU': 'D', 'GAC': 'D', 'GAA': 'E',
            'GAG': 'E', 'UGU': 'C', 'UGC': 'C', 'UGG': 'W', 'CGU': 'R',
            'CGC': 'R', 'CGA': 'R', 'CGG': 'R', 'AGU': 'S', 'AGC': 'S',
            'AGA': 'R', 'AGG': 'R', 'GGU': 'G', 'GGC': 'G', 'GGA': 'G',
            'GGG': 'G', 'UAA': '*', 'UAG': '*', 'UGA': '*'
        }
        """ Inicjalizacja pustej sekwencji białka """
        protein = ""  
        """ Iteracja przez sekwencję RNA w tripletach """
        for i in range(0, len(self.data) - 2, 3):
            """ Pobranie tripletu (kodonu) """
            codon = self.data[i:i + 3]  
            
            if codon in codon_table:
                """ Dodanie odpowiedniego aminokwasu do sekwencji białka """
                protein += codon_table[codon]   
                """ Zwraca nową sekwencję białka """
        return ProteinSequence(self.identifier + "_przetłumaczona", protein)  


""" Definiowanie klasy ProteinSequence"""
class ProteinSequence:
    """ Zbiór dozwolonych znaków w sekwencji białka """
    VALID_CHARS = set('ACDEFGHIKLMNPQRSTVWY*')

    """ Metoda inicjalizacyjna klasy """
    def __init__(self, identifier, data):
        """ Przypisanie identyfikatora sekwencji """
        self.identifier = identifier  
        """ Przypisanie sekwencji aminokwasów """
        self.data = data  
        """ Obliczenie długości sekwencji """
        self.length = len(data)  
        """ Sprawdzanie poprawności sekwencji """
        self.validate_sequence()  

    """ Metoda sprawdzająca poprawność sekwencji """
    def validate_sequence(self):
        """ Sprawdzanie, czy każda zasada w sekwencji należy do dozwolonych znaków """
        if not all(char in self.VALID_CHARS for char in self.data):
            """ Wyjątek w przypadku niedozwolonych znaków """
            raise ValueError("Sekwencja białka zawiera niedozwolone znaki.")  

    """ Metoda zwracająca sekwencję w formacie FASTA """
    def __str__(self):
        """ # Format FASTA: identyfikator na pierwszej linii, sekwencja na drugiej"""
        return f">{self.identifier}\n{self.data}"  
    """ Metoda do mutowania sekwencji """
    def mutate(self, position, value):
        """ Sprawdzanie, czy nowy znak jest dozwolony """
        if value not in self.VALID_CHARS:
            """ Wyjątek w przypadku niedozwolonego znaku """
            raise ValueError("Niedozwolony znak do mutacji.") 
            
        """ Zmiana aminokwasu na nową wartość w podanej pozycji """
        self.data = self.data[:position] + value + self.data[position + 1:]

    """ Metoda do wyszukiwania motywu w sekwencji """
    def findMotif(self, motif):
        """ Zwraca pozycję początkową motywu """
        return self.data.find(motif)  


""" Funkcja testująca klasy """
def test_sequences():
    """ Tworzenie obiektu klasy DNASequence """
    dna = DNASequence("seq1", "ATGCGT")
    """ Wyświetlanie sekwencji DNA w formacie FASTA """
    print(dna)  
    
    """ Mutacja sekwencji DNA """
    dna.mutate(1, 'A')
    """ Wyświetlanie zmutowanej sekwencji DNA """
    print(dna)  

    """ Szukanie motywu w sekwencji DNA. Wyświetlanie pozycji motywu"""
    print(dna.findMotif("GCG"))

    """ Tworzenie nici komplementarnej """
    complement_dna = dna.complement()
    """ Wyświetlanie nici komplementarnej """
    print(complement_dna)  

    """ Transkrypcja sekwencji DNA do RNA """
    rna = dna.transcribe()
    """ Wyświetlanie sekwencji RNA w formacie FASTA """
    print(rna)  

    """ Mutacja sekwencji RNA """
    rna.mutate(1, 'U')
    """  Wyświetlanie zmutowanej sekwencji RNA """
    print(rna)  

    """ Szukanie motywu w sekwencji RNA. Wyświetlanie pozycji motywu"""
    print(rna.findMotif("UGC"))   

    """ Translacja sekwencji RNA do białka """
    protein = rna.translate()
    """ Wyświetlanie sekwencji białka w formacie FASTA """
    print(protein)   

    """ Mutacja sekwencji białka """
    protein.mutate(0, 'M')
    """ Wyświetlanie zmutowanej sekwencji białka """
    print(protein)  

    """ Szukanie motywu w sekwencji białka. Wyświetlanie pozycji motywu """
    print(protein.findMotif("MCG"))  

""" Uruchamianie funkcji testującej """
test_sequences()
